/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: sahmad
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Implementation
//!	Generated Date	: Wed, 31, Oct 2018  
	File Path	: DefaultComponent\DefaultConfig\Implementation.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Implementation.h"
//## auto_generated
#include "DriveTestApp.h"
//#[ ignore
#define evDrive_SERIALIZE OM_NO_OP

#define evDrive_UNSERIALIZE OM_NO_OP

#define evDrive_CONSTRUCTOR evDrive()

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()
//#]

//## package Implementation


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Implementation, Implementation)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event evDrive()
evDrive::evDrive() {
    NOTIFY_EVENT_CONSTRUCTOR(evDrive)
    setId(evDrive_Implementation_id);
}

bool evDrive::isTypeOf(const short id) const {
    return (evDrive_Implementation_id==id);
}

IMPLEMENT_META_EVENT_P(evDrive, Implementation, Implementation, evDrive())

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_Implementation_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_Implementation_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, Implementation, Implementation, evStop())

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Implementation.cpp
*********************************************************************/
