/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: sahmad
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DriveTestApp
//!	Generated Date	: Wed, 31, Oct 2018  
	File Path	: DefaultComponent\DefaultConfig\DriveTestApp.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "DriveTestApp.h"
//#[ ignore
#define Implementation_DriveTestApp_DriveTestApp_SERIALIZE OM_NO_OP

#define Implementation_DriveTestApp_Ramp_SERIALIZE aomsmethod->addAttribute("p_Target", x2String(p_Target));

#define OMAnim_Implementation_DriveTestApp_setTarget_int_UNSERIALIZE_ARGS OP_UNSER(OMDestructiveString2X,p_Target)

#define OMAnim_Implementation_DriveTestApp_setTarget_int_SERIALIZE_RET_VAL
//#]

//## package Implementation

//## class DriveTestApp
DriveTestApp::DriveTestApp(IOxfActive* theActiveContext) : Actual(0), Target(0) {
    NOTIFY_REACTIVE_CONSTRUCTOR(DriveTestApp, DriveTestApp(), 0, Implementation_DriveTestApp_DriveTestApp_SERIALIZE);
    setActiveContext(theActiveContext, false);
    initStatechart();
}

DriveTestApp::~DriveTestApp() {
    NOTIFY_DESTRUCTOR(~DriveTestApp, true);
    cancelTimeouts();
}

void DriveTestApp::Ramp(int p_Target) {
    NOTIFY_OPERATION(Ramp, Ramp(int), 1, Implementation_DriveTestApp_Ramp_SERIALIZE);
    //#[ operation Ramp(int)
    if(Actual < p_Target )
    {
    	Actual++;
    }
    else if(Actual > p_Target ) 
    {
    	Actual--;
    }
    //#]
}

int DriveTestApp::getActual() const {
    return Actual;
}

void DriveTestApp::setActual(int p_Actual) {
    Actual = p_Actual;
    NOTIFY_SET_OPERATION;
}

int DriveTestApp::getBrake() const {
    return Brake;
}

void DriveTestApp::setBrake(int p_Brake) {
    Brake = p_Brake;
    NOTIFY_SET_OPERATION;
}

int DriveTestApp::getTarget() const {
    return Target;
}

void DriveTestApp::setTarget(int p_Target) {
    Target = p_Target;
    NOTIFY_SET_OPERATION;
}

bool DriveTestApp::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void DriveTestApp::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    rootState_timeout = NULL;
}

void DriveTestApp::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool DriveTestApp::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

void DriveTestApp::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.standby");
        rootState_subState = standby;
        rootState_active = standby;
        //#[ state standby.(Entry) 
           Brake = false;
        //#]
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus DriveTestApp::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State standby
        case standby:
        {
            if(IS_EVENT_TYPE_OF(evDrive_Implementation_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    //#[ state standby.(Exit) 
                       Brake = true;
                    //#]
                    NOTIFY_STATE_EXITED("ROOT.standby");
                    NOTIFY_STATE_ENTERED("ROOT.drive");
                    rootState_subState = drive;
                    rootState_active = drive;
                    rootState_timeout = scheduleTimeout(100, "ROOT.drive");
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State drive
        case drive:
        {
            if(IS_EVENT_TYPE_OF(evStop_Implementation_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    cancel(rootState_timeout);
                    NOTIFY_STATE_EXITED("ROOT.drive");
                    NOTIFY_STATE_ENTERED("ROOT.stopping");
                    pushNullTransition();
                    rootState_subState = stopping;
                    rootState_active = stopping;
                    rootState_timeout = scheduleTimeout(100, "ROOT.stopping");
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("4");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.drive");
                            //#[ transition 4 
                            Ramp(Target);
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.drive");
                            rootState_subState = drive;
                            rootState_active = drive;
                            rootState_timeout = scheduleTimeout(100, "ROOT.drive");
                            NOTIFY_TRANSITION_TERMINATED("4");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State stopping
        case stopping:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("5");
                            popNullTransition();
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.stopping");
                            //#[ transition 5 
                            Ramp(0);
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.stopping");
                            pushNullTransition();
                            rootState_subState = stopping;
                            rootState_active = stopping;
                            rootState_timeout = scheduleTimeout(100, "ROOT.stopping");
                            NOTIFY_TRANSITION_TERMINATED("5");
                            res = eventConsumed;
                        }
                }
            else if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 3 
                    if(Actual==0)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            popNullTransition();
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.stopping");
                            //#[ transition 3 
                             Target = 0;
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.standby");
                            rootState_subState = standby;
                            rootState_active = standby;
                            //#[ state standby.(Entry) 
                               Brake = false;
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDriveTestApp::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("Actual", x2String(myReal->Actual));
    aomsAttributes->addAttribute("Brake", x2String(myReal->Brake));
    aomsAttributes->addAttribute("Target", x2String(myReal->Target));
}

void OMAnimatedDriveTestApp::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case DriveTestApp::standby:
        {
            standby_serializeStates(aomsState);
        }
        break;
        case DriveTestApp::drive:
        {
            drive_serializeStates(aomsState);
        }
        break;
        case DriveTestApp::stopping:
        {
            stopping_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedDriveTestApp::stopping_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.stopping");
}

void OMAnimatedDriveTestApp::standby_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.standby");
}

void OMAnimatedDriveTestApp::drive_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.drive");
}
//#]

IMPLEMENT_REACTIVE_META_P(DriveTestApp, Implementation, Implementation, false, OMAnimatedDriveTestApp)

IMPLEMENT_META_OP(OMAnimatedDriveTestApp, Implementation_DriveTestApp_setTarget_int, "setTarget", FALSE, "setTarget(int)", 1)

IMPLEMENT_OP_CALL(Implementation_DriveTestApp_setTarget_int, DriveTestApp, setTarget(p_Target), NO_OP())
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\DriveTestApp.cpp
*********************************************************************/
