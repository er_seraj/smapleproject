/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: sahmad
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DriveTestApp
//!	Generated Date	: Wed, 31, Oct 2018  
	File Path	: DefaultComponent\DefaultConfig\DriveTestApp.h
*********************************************************************/

#ifndef DriveTestApp_H
#define DriveTestApp_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "Implementation.h"
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//#[ ignore
#define OMAnim_Implementation_DriveTestApp_setTarget_int_ARGS_DECLARATION int p_Target;
//#]

//## package Implementation

//## class DriveTestApp
class DriveTestApp : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDriveTestApp;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    DriveTestApp(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~DriveTestApp();
    
    ////    Operations    ////
    
    //## operation Ramp(int)
    void Ramp(int p_Target);
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getActual() const;
    
    //## auto_generated
    void setActual(int p_Actual);
    
    //## auto_generated
    int getBrake() const;
    
    //## auto_generated
    void setBrake(int p_Brake);
    
    //## auto_generated
    int getTarget() const;
    
    //## auto_generated
    void setTarget(int p_Target);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);
    
    ////    Attributes    ////
    
    int Actual;		//## attribute Actual
    
    int Brake;		//## attribute Brake
    
    int Target;		//## attribute Target
    
    ////    Framework operations    ////
    
    ////    Framework    ////

public :

    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // stopping:
    //## statechart_method
    inline bool stopping_IN() const;
    
    // standby:
    //## statechart_method
    inline bool standby_IN() const;
    
    // drive:
    //## statechart_method
    inline bool drive_IN() const;

protected :

//#[ ignore
    enum DriveTestApp_Enum {
        OMNonState = 0,
        stopping = 1,
        standby = 2,
        drive = 3
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    IOxfTimeout* rootState_timeout;
//#]
};

#ifdef _OMINSTRUMENT
DECLARE_OPERATION_CLASS(Implementation_DriveTestApp_setTarget_int)

//#[ ignore
class OMAnimatedDriveTestApp : virtual public AOMInstance {
    DECLARE_REACTIVE_META(DriveTestApp, OMAnimatedDriveTestApp)
    
    DECLARE_META_OP(Implementation_DriveTestApp_setTarget_int)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void stopping_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void standby_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void drive_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool DriveTestApp::rootState_IN() const {
    return true;
}

inline bool DriveTestApp::stopping_IN() const {
    return rootState_subState == stopping;
}

inline bool DriveTestApp::standby_IN() const {
    return rootState_subState == standby;
}

inline bool DriveTestApp::drive_IN() const {
    return rootState_subState == drive;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\DriveTestApp.h
*********************************************************************/
